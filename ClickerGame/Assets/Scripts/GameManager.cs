﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance { get { return _instance; } }

    [SerializeField, Tooltip("Tiempo de actualización en segundos, por defecto 1 minuto")] float updateRate = 60.0f;
    Dictionary<ResourceController, ResourceCost> resourceControllers; //todo: refactor this
    [SerializeField] ResourceController woodController, ironController, foodController; //todo: refactor this
    [SerializeField] CameraController cameraController;
    [SerializeField] TextMeshProUGUI humanText;
    [Header("UI")]
    [SerializeField] GameObject BuildPanel;
    //private
    private float currentTime;
    private static GameManager _instance;
    private Build selectedBuild;

    private List<Build> builds;
    GameManager()
    {
        builds = new List<Build>(10);

    }
    private void Awake()
    {
        _instance = this;
        resourceControllers = new Dictionary<ResourceController, ResourceCost>(3);

        resourceControllers.Add(woodController, new ResourceCost());
        resourceControllers.Add(ironController, new ResourceCost());
        resourceControllers.Add(foodController, new ResourceCost());
    }

    void Update()
    {
        currentTime += Time.deltaTime;

        if (currentTime < updateRate) return;

        currentTime = 0;

        foreach (var item in resourceControllers)
        {
            item.Key.OnResourceUpdate?.Invoke(item.Value.cost);
        }
        humanText.text = ((CKSaveData)CKSaveData.currentData).currentHumans.ToString();
        SaveGame();
    }

    public void AddResourceCost(int cost, EResourceType type, Build build)
    {
        switch (type)
        {
            case EResourceType.Wood:
                {
                    ResourceCost data = resourceControllers[woodController];
                    data.cost += cost;
                    resourceControllers[woodController] = data;
                    break;
                }
            case EResourceType.Iron:
                {
                    ResourceCost data = resourceControllers[ironController];
                    data.cost += cost;
                    resourceControllers[ironController] = data;
                break;
                }
            case EResourceType.Food:
                {
                    ResourceCost data = resourceControllers[foodController];
                    data.cost += cost;
                    resourceControllers[foodController] = data;
                    break;
                }
            default:
                break;
        }
    }
    public void AddResource(int val, EResourceType type)
    {
        switch (type)
        {
            case EResourceType.Wood:
                woodController.AddResource(val);
                break;
            case EResourceType.Iron:
                ironController.AddResource(val);
                break;
            case EResourceType.Food:
                foodController.AddResource(val);
                break;
            default:
                break;
        }
        SaveGame();
    }

    public void SelectBuild(Build current)
    {
        if (current == selectedBuild) return;
        selectedBuild = current;

        cameraController.SetFollow(current ? current.transform : null);

        if (current)
        {
            BuildPanel.gameObject.SetActive(true);
            //update info text
            var controller = BuildPanel.GetComponent<BuildPanelController>();
            controller.UpdatePanel(selectedBuild);
            controller.AddExitAction(UnSelect);
        }
        else

        {
            BuildPanel.gameObject.SetActive(true);

        }
    }
    public void UnSelect()
    {
        cameraController.SetFollow(null);
        selectedBuild = null;
    }
    public void RegisterBuild(Build build)
    {
        //se ha creado un edificio, actualizaremos los costes de produccion de cada edificio
        builds.Add(build);
    }

    public void OnLoadGame()
    {
        string SlotName = PlayerPrefs.GetString(GameConstants.CSKey);
        CKSaveData.currentData = (CKSaveData)SerializationManager.Load(SlotName);
    }
    public void SaveGame()
    {
        string SlotName = PlayerPrefs.GetString(GameConstants.CSKey);
        var save = (CKSaveData)CKSaveData.currentData;
        SerializationManager.Save(SlotName, save);
    }
    public void ExitToMenu()
    {
        SaveGame();
        SceneManager.LoadScene("StartMenu");
    }
}
