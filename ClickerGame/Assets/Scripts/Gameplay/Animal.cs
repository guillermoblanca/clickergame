﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Animal : MonoBehaviour
{
    [SerializeField]ResourceCost resource;
    [SerializeField] int clicksForDestroy = 3;
    [SerializeField] GameObject costPanelPrefab;
    [SerializeField] int experience =100;
    int currentClicks = 0;
    private void OnMouseDown()
    {
        currentClicks++;

        GameObject costPanel = Instantiate(costPanelPrefab, transform, false);
        costPanel.GetComponentInChildren<TextMeshProUGUI>().text = "-1";

        //todo: cambiar a efecto de salto con el dojump

        costPanel.GetComponent<RectTransform>().DOLocalMoveY(-60, 1.0f);
        Destroy(costPanel, 1.1f);

        if (currentClicks >= clicksForDestroy)
        {
            GameManager.instance.AddResource(resource.cost, resource.type);

            CKSaveData.currentData.profile.experience += experience;
            Destroy(gameObject);
        }
    }
}
