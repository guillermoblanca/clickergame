﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] GameObject SpawnPrefab;
    [SerializeField] int spawnPerRound = 4;
    [SerializeField] float TimeToSpawn = 2.0f;
    [SerializeField] Vector3 SpawnArea = new Vector3(10,0,10);
    [SerializeField] int spawnMax = 15;

    List<GameObject> childs;
    
    float currentTimer = 0;
    // Start is called before the first frame update
    void Start()
    {
        childs = new List<GameObject>();
        SpawnCreatures();
        
    }

    // Update is called once per frame
    void Update()
    {
        currentTimer += Time.deltaTime;
        if (currentTimer > TimeToSpawn)
        {
            currentTimer = 0;

            for (int i = 0; i < childs.Count; i++)
            {
                if (childs[i] == null)
                    childs.RemoveAt(i);
            }
            if (childs.Count>= spawnMax)
                return;

            SpawnCreatures();
        }
    }

    void SpawnCreatures()
    {
        for (int i = 0; i < spawnPerRound; i++)
        {
            float x = Random.Range(-SpawnArea.x, SpawnArea.x);
            float z = Random.Range(-SpawnArea.z, SpawnArea.z);

            Vector3 spawnPos = new Vector3(transform.position.x + x, transform.position.y, transform.position.z + z);
            GameObject spawned = Instantiate(SpawnPrefab, spawnPos, Quaternion.identity, transform);
            childs.Add(spawned);

        }
    }

}
