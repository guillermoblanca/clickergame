﻿using UnityEngine;
public class Build : MonoBehaviour
{
    /*Costes minimos del edificio*/
    [SerializeField] ResourceCost[] Costs;
    [SerializeField] int RequieredHuman = 0;
    [SerializeField] int Level = 1;
    [SerializeField] string BuildName;
    [SerializeField] string BuildDetail;
    [SerializeField] Material GhostMaterial;
    [SerializeField] Material IdleMaterial;


    public ResourceCost[] GetCost()
    {
        return Costs;
    }
    private void Start()
    {
        for (int i = 0; i < Costs.Length; i++)
        {

            GameManager.instance.AddResourceCost(Costs[i].cost, Costs[i].type, this);
        }
    }
    private void OnDestroy()
    {
        for (int i = 0; i < Costs.Length; i++)
        {

            GameManager.instance.AddResourceCost(-Costs[i].cost, Costs[i].type, this);
        }
    }
    //acccesors
    #region Accesor methods

    public string GetBuildName() { return BuildName; }
    public string GetBuildDetail() { return BuildDetail; }
    public int GetBuildLevel() { return Level; }
    public ResourceCost[] GetResourceCosts() { return Costs; }
    #endregion


    public bool CreateBuild() { return false; }

    public bool UpdateCost(ResourceCost[] resourceCosts, int totalHuman)
    {

        return false;
    }

    public void UpgradeBuild()
    {
        //remove cost and then we upgrade to next level
        //todo: create a table for this
        //todo: create method
    }


    private void RegisterBuild()
    {
        /*Registra los costes del edificio en el gamemanager*/
    }
    private void OnMouseDown()
    {
        GameManager.instance.SelectBuild(this);
    }
}