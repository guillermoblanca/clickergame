﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartManager : MonoBehaviour
{
    // Start is called before the first frame update
    PlayerProfile profile = new PlayerProfile();
    private void Start()
    {
        //has savegame?

    }
    public void NewGame(bool force)
    {
        if(profile.name == string.Empty)
        {
            //error message 
            Debug.LogWarning("There is no name for the slot, choose one before continue...");
            return;
        }

        if (!force && Directory.Exists(Application.persistentDataPath + "/saves/" + profile.name + ".save")) //exist the slot
        {
            //popup message

            //return;
        }

        profile.experience = 0;
        profile.level = 1;
        CKSaveData data = new CKSaveData();
        data.profile = profile;
        data.currentHumans = 0;
        data.wood = new ResourceData(100, 100);
        data.iron = new ResourceData(100, 100);
        data.food = new ResourceData(100, 100);
        CKSaveData.currentData = data;

        PlayerPrefs.SetString("CurrentSlot", profile.name);
        
        SerializationManager.Save(profile.name, data);
        SceneManager.LoadScene("UI_TEST");

    }
    public void SetNewPlayer(string playername)
    {
        profile.name = playername;
    }
    public void ChangeLanguage(SystemLanguage language)
    {

    }
}
