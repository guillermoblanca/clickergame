/*
@Author by Guillermo Blanca

*/
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public static class SerializationManager
{
    /*
    Guarda la partida. Devuelve verdadero cuando ha guardado la partida. 
    Los datos se almacenan en un archivo dentro de APPDATA en la carpeta saves. 
    @param SlotName representa el nombre del archivo guardado.
    @param SaveData representa cualquier dato que pueda ser serializado 
    */
    public static bool Save(string SlotName, object SaveData)
    {
        BinaryFormatter formatter = GetBinaryFormatter();

        string savePath = Application.persistentDataPath+ "/saves/";
        if (!Directory.Exists(savePath))
        {
            Directory.CreateDirectory(savePath);
        }

        string path =  savePath +SlotName + ".save";

        FileStream file = File.Create(path);
        formatter.Serialize(file, SaveData);

        file.Close();

        return true;
    }
    /*
    Carga los datos de la carpeta APPDATA/saves/ devuelve el objeto que se haya cargado
    es necesario hacer un cast para extrar la estructura de datos específica.
    
    @param SlotName representa el nombre del archivo guardado
    */
    public static object Load(string SlotName)
    {
        string savePath = Application.persistentDataPath+ "/saves/";

        string path = savePath+ SlotName + ".save";
        if (!File.Exists(path))
        {
            Debug.LogError("File doesnt exist! " + SlotName);
            return null;
        }
        BinaryFormatter formatter = GetBinaryFormatter();
        FileStream file = File.Open(path, FileMode.Open);

        try
        {

            object loadObject = formatter.Deserialize(file);
            file.Close();

            return loadObject;
        }
        catch
        {
            Debug.LogError("Error loading " + SlotName);
            file.Close();
            return null;
        }

    }
    /*
    Sirve para los métodos Save/Load para iniciar el generador de archivos binarios
    */
    public static BinaryFormatter GetBinaryFormatter()
    {
        BinaryFormatter formatter = new BinaryFormatter();

        return formatter;
    }
}