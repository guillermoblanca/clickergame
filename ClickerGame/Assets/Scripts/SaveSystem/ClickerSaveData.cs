﻿


using System;
using UnityEngine;
[System.Serializable]
public struct ResourceData
{
    public ResourceData(int max, int current)
    {
        maxType = max;
        currentType = current;
        ResourceDelta = null;
    }

    private event Action<int> ResourceDelta;
    public int currentType;
    public int maxType;

    public void ModifyType(int add)
    {
        currentType = Mathf.Clamp(currentType + add, 0, maxType);
        ResourceDelta?.Invoke(currentType / maxType);

    }
    public void ModifyMaxType(int add)
    {
        maxType += add;

    }
    public void SubscribeDelta(Action<int> action)
    {
        ResourceDelta += action;
    }
    public void UnsubscribeDelta(Action<int> action)
    {
        ResourceDelta -= action;
    }
}

[System.Serializable]
public class CKSaveData : SaveData
{
    public ResourceData wood;
    public ResourceData iron;
    public ResourceData food;
    public int currentHumans;

    public ResourceData GetResourceData(EResourceType type)
    {
        switch (type)
        {
            case EResourceType.Wood:
                return wood;
            case EResourceType.Iron:
                return iron;
            case EResourceType.Food:
                return food;

        }
        return new ResourceData(-1, -1);
    }
    public void SetResourceData(EResourceType type, int current, int max)
    {
        switch (type)
        {
            case EResourceType.Wood:
                {
                    wood.currentType= current;
                    wood.maxType = max;
                }
                break;
            case EResourceType.Iron:
                {
                    iron.currentType = current;
                    iron.maxType = max;
                }
                break;
            case EResourceType.Food:
                {
                    food.currentType = current;
                    iron.maxType = max;
                }
                break;
            default:
                break;

               
        }
    }

}