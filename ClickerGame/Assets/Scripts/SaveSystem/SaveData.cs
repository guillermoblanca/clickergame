﻿

[System.Serializable]
/*Perfil del jugador con datos básicos*/
public class PlayerProfile
{
    public string name;
    public int level;
    public int experience;

}

[System.Serializable]
/*Clase base de la que heredan todos los datos que se quieran guardar */
public class SaveData
{
    
    private static SaveData _currentData;
    //datos actuales de la partida. El GameManager debe ser el que inicialice estos datos y el que los guarde. Se puede realizar un Cast a clases personalizadas
    public static SaveData currentData;

    public PlayerProfile profile;
    //devuelve si existen los datos guardados de manera local 
    public static bool HasSaveData() {return currentData != null;}
    
}

