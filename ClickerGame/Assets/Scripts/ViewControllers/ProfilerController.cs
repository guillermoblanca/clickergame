﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ProfilerController : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI NameText;
    [SerializeField] TextMeshProUGUI LevelText;
    [SerializeField] TextMeshProUGUI ExperienceText;

    public void OnEnable()
    {
        NameText.text = "Name: " + SaveData.currentData.profile.name;
        LevelText.text ="Level: "+ SaveData.currentData.profile.level.ToString();
        ExperienceText.text = "Exp: " +SaveData.currentData.profile.experience.ToString();
    }
}
