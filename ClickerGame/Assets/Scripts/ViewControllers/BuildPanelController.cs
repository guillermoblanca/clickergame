﻿using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;

public class BuildPanelController : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI NameText;
    [SerializeField] TextMeshProUGUI DetailText;
    [SerializeField] Button ExitButton;
    [SerializeField] GameObject costPanel;

    private void Start()
    {
        ExitButton.onClick.AddListener(() => { gameObject.SetActive(false); });
    }

    public void AddExitAction(UnityAction action)
    {
        ExitButton.onClick.AddListener(action);
    }

    public void UpdatePanel(Build build)
    {
        var canvas = GetComponent<CanvasGroup>();
        canvas.alpha = 0;
        canvas.DOFade(1.0f, 0.5f);

        var costs = build.GetCost();
        int childsCount = costPanel.transform.GetChildCount();
        for (int i = 1; i < childsCount; i++)
        {
            Destroy(costPanel.transform.GetChild(i).gameObject);
        }

        for (int i = 0; i < costs.Length; i++)
        {
            var childCost = Instantiate(new GameObject("cost"), costPanel.transform);
            var textCost = childCost.AddComponent<TextMeshProUGUI>();
            textCost.text = costs[i].type.ToString() + ": " + costs[i].cost;
            textCost.fontSize = 16;

        }

        NameText.text = build.GetBuildName();
        DetailText.text = build.GetBuildDetail();

    }

}
