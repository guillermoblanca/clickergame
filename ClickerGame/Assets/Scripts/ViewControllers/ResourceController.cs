﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ResourceController : MonoBehaviour
{
    [SerializeField] int currentResource;
    [SerializeField] int MaxResource;
    [SerializeField] EResourceType type;

    [SerializeField] TextMeshProUGUI ResourceText;
    [SerializeField] Slider ResourceSlider;
    [SerializeField] Image ResourceImage;
    [SerializeField] GameObject costPanelPrefab;

    public ResourceEvent OnResourceUpdate;
    public ResourceController()
    {
        currentResource = 100;
        MaxResource = 100;
    }
    private void Start()
    {
        //todo: refactor this 
        Load();
        SetResources(currentResource, MaxResource);
        OnResourceUpdate.AddListener(RegisterCost);


    }

    public void SetResources(float currentValue, float maxValue)
    {
        ResourceText.text = currentValue + "/" + maxValue;

        float delta = currentValue / maxValue;
        ResourceSlider.DOValue(delta, 1.0f);
        Save();

    }
    public void RegisterCost(float amount)
    {
        if (amount == 0) return;
        int diff = currentResource - (int)amount;

        bool hasResource = diff > 0;
        if (!hasResource)
        {
            //do shake effect
            GetComponent<RectTransform>().DOShakePosition(2.5f, 2.0f);
            return;
        }
        currentResource = diff;
        GameObject costPanel = Instantiate(costPanelPrefab, transform, false);
        costPanel.GetComponentInChildren<TextMeshProUGUI>().text = "-" + amount.ToString();

        //todo: cambiar a efecto de salto con el dojump

        costPanel.GetComponent<RectTransform>().DOLocalMoveY(-60, 1.0f);
        Destroy(costPanel, 1.1f);

        SetResources(currentResource, MaxResource);
    }
    public void AddResource(int amount)
    {
        if (amount == 0) return;
        int val = Mathf.Clamp(currentResource + amount, 0, MaxResource);
        currentResource = val;

        SetResources(currentResource, MaxResource);
        GameObject costPanel = Instantiate(costPanelPrefab, transform, false);
        costPanel.GetComponentInChildren<TextMeshProUGUI>().text = "+" + amount.ToString();

        //todo: cambiar a efecto de salto con el dojump

        costPanel.GetComponent<RectTransform>().DOLocalMoveY(-60, 1.0f);
        Destroy(costPanel, 1.1f);

        SetResources(currentResource, MaxResource);
    }


    //write/save data

    public void Save()
    {
        ((CKSaveData)CKSaveData.currentData).SetResourceData(type, currentResource, MaxResource);
    }
    public void Load()
    {
        CKSaveData data = (CKSaveData)CKSaveData.currentData;
        var resource = data.GetResourceData(type);
        currentResource = resource.currentType;
        MaxResource = resource.maxType;
    }
}
