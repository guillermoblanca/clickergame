﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class SlotController : MonoBehaviour
{
    List<string> slotNames;
    public Button prefabButton;
    public Button[] buttons;
    public void Start()
    {
        string savePath = Path.Combine(Application.persistentDataPath, "saves");
        if (!Directory.Exists(savePath)) return;

        string[] paths = Directory.GetFiles(savePath);

        slotNames = new List<string>();
        for (int i = 0; i < paths.Length; i++)
        {
            string slot = Path.GetFileName(paths[i]);
            slot = slot.Remove(slot.Length - 5); //we remove .save
            slotNames.Add(slot);
             Button newButton  = Instantiate(prefabButton, transform, false) as Button;
            newButton.GetComponentInChildren<TextMeshProUGUI>().text = slot;
            newButton.onClick.AddListener
                (
                () =>
                {
                    CKSaveData save =  (CKSaveData) SerializationManager.Load(slot);
                    PlayerPrefs.SetString(GameConstants.CSKey, save.profile.name);
                    CKSaveData.currentData = save;
                    SceneManager.LoadScene("UI_Test");
                }

                );
            Debug.Log("slot[ " + i + "] " + slot);
        }

    }


}
