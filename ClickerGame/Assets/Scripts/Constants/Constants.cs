﻿using UnityEngine.Events;

public enum EResourceType
{
    Wood,
    Iron,
    Food
}
[System.Serializable]
public struct ResourceCost
{
    public int cost;
    public EResourceType type;

}
public static class GameConstants
{
    public static readonly float UpdateRate = 60.0f;
    public static readonly string CSKey = "CurrentSlot"; //currentslot name
}


//Events
[System.Serializable]
public class ResourceEvent : UnityEvent<float>
{

}