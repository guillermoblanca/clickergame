﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*Tareas:
 
* Añadir límites al efecto zoom y al movimiento

 */
public class CameraController : MonoBehaviour
{
    [SerializeField] Transform cameraTransform;
    [SerializeField] float normalSpeed = 1.0f;
    [SerializeField] float fastSpeed = 6.0f;
    [SerializeField] float movementSpeed;
    [SerializeField] float movementTime;
    [SerializeField] float rotationAmount = 10.0f;
    [SerializeField] Vector3 zoomAmount = new Vector3(0, -10, 10);
    [SerializeField] Vector3 dragStartPosition;

    Vector3 dragCurrentPosition;

    Vector3 newPosition;
    Quaternion newRotation;
    Vector3 newZoom;

    Vector3 rotateStartPosition;
    Vector3 rotateCurrentPosition;
    Transform followTransform;
    CameraController()
    {
        movementSpeed = 1.0f;
        movementTime = 1.0f;
    }
    public void SetFollow(Transform followObject)
    {
        followTransform = followObject;
    }
    private void Start()
    {
        movementSpeed = normalSpeed;
        newPosition = transform.position;
        newRotation = transform.rotation;
        newZoom = cameraTransform.localPosition;

    }
    private void FixedUpdate()
    {
        if (followTransform)
        {
            transform.position = Vector3.Lerp(transform.position, followTransform.position,Time.deltaTime * movementTime);
        }
        else
        {
            HandleMouseInput();
            HandleMovementInput();
        }
    }
    void HandleMovementInput()
    {
        //   float moveForward = Input.GetAxis("Forward");
        //   float moveRight = Input.GetAxis("Right");

        if (Input.GetKey(KeyCode.W))
        {
            newPosition += (transform.forward * movementSpeed);

        }
        if (Input.GetKey(KeyCode.S))
        {
            newPosition += (transform.forward * -movementSpeed);
        }
        if (Input.GetKey(KeyCode.A))
        {
            newPosition += transform.right * -movementSpeed;
        }
        if (Input.GetKey(KeyCode.D))
        {
            newPosition += transform.right * movementSpeed;
        }

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            movementSpeed = fastSpeed;
        }
        else if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            movementSpeed = normalSpeed;
        }

        if (Input.GetKey(KeyCode.Q))
        {
            newRotation *= Quaternion.Euler(Vector3.up * rotationAmount);
        }
        if (Input.GetKey(KeyCode.E))
        {
            newRotation *= Quaternion.Euler(Vector3.up * -rotationAmount);
        }
        if (Input.GetKey(KeyCode.R))
        {
            newZoom += zoomAmount;

        }
        if (Input.GetKey(KeyCode.F))
        {
            newZoom -= zoomAmount;
        }
        transform.position = Vector3.Lerp(transform.position, newPosition, Time.deltaTime * movementTime);
        transform.rotation = Quaternion.Lerp(transform.rotation, newRotation, Time.deltaTime * movementTime);
        cameraTransform.localPosition = Vector3.Lerp(cameraTransform.localPosition, newZoom, Time.deltaTime * movementTime);

    }
    void HandleMouseInput()
    {
        if (Input.mouseScrollDelta.y != 0)
        {
            newZoom += Input.mouseScrollDelta.y * zoomAmount;
        }
        if (Input.GetMouseButtonDown(0))
        {
            Plane plane = new Plane(Vector3.up, Vector3.zero);
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (plane.Raycast(ray, out float entry))
            {
                dragStartPosition = ray.GetPoint(entry);
            }
        }
        if (Input.GetMouseButton(0))
        {
            Plane plane = new Plane(Vector3.up, Vector3.zero);
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (plane.Raycast(ray, out float entry))
            {
                dragCurrentPosition = ray.GetPoint(entry);
                newPosition = transform.position + dragStartPosition - dragCurrentPosition;
            }
        }
        if (Input.GetMouseButtonDown(2))
        {
            rotateStartPosition = Input.mousePosition;
        }
        if (Input.GetMouseButton(2))
        {
            rotateCurrentPosition = Input.mousePosition;
            Vector3 diff = rotateStartPosition - rotateCurrentPosition;
            rotateStartPosition = rotateCurrentPosition;
            newRotation *= Quaternion.Euler(Vector3.up * (-diff.x / 5.0f));
        }
    }
}
